package me.aecsocket.guiframework;

import me.aecsocket.guiframework.gui.GUIView;
import org.bukkit.inventory.InventoryView;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

/** The main plugin class. */
public class GUIFramework extends JavaPlugin {
    /** An instance of this plugin. */
    private static GUIFramework instance;
    /** A {@link Map} of currently open {@link GUIView}s linked to {@link InventoryView}s. */
    private static Map<InventoryView, GUIView> views;

    /** Gets an instance of this plugin.
     * @return An instance of this plugin.
     */
    public static GUIFramework getInstance() { return instance; }

    /** Gets a {@link Map} of currently open {@link GUIView}s linked to {@link InventoryView}s.
     @return A {@link Map} of currently open {@link GUIView}s linked to {@link InventoryView}s.
     */
    public static Map<InventoryView, GUIView> getViews() { return views; }

    @Override
    public void onEnable() {
        instance = this;
        views = new HashMap<>();

        getServer().getPluginManager().registerEvents(new EventHandle(), this);
    }

    /** Gets a {@link GUIView} from an {@link InventoryView} if it is a GUIView.
     @param view The {@link InventoryView}.
     @return The {@link GUIView} if it is a valid one, or null if it is not.
     */
    public static GUIView getView(InventoryView view) {
        return views.getOrDefault(view, null);
    }
}
