package me.aecsocket.guiframework.gui;

import org.bukkit.entity.Player;

/** A custom menu for players. */
public interface GUI {
    /** Shows a {@link Player} the GUI.
     @param player The {@link Player}.
     @return The {@link GUIView}.
     */
    GUIView open(Player player);
}
