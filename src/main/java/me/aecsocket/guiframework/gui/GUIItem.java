package me.aecsocket.guiframework.gui;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/** An item used in a {@link GUIView}. */
public interface GUIItem {
    /** Gets the {@link ItemStack} as it appears in an inventory.
    @param player The player that this is being generated to.
    @return The {@link ItemStack} as it appears in an inventory.
     */
    ItemStack getHandle(Player player);

    /** Ran when this item is clicked in a GUI.
     @param event The {@link InventoryClickEvent}.
     @param view The {@link GUIView} it was clicked in.
     */
    void onClick(InventoryClickEvent event, GUIView view);
}
