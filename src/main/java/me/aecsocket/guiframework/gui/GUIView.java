package me.aecsocket.guiframework.gui;

import org.bukkit.entity.Player;
import org.bukkit.inventory.InventoryView;

import java.util.Map;

/** A wrapper around {@link InventoryView} and {@link GUI}. */
public class GUIView {
    /** The {@link GUI}. */
    private final GUI gui;
     /** The {@link InventoryView}. */
    private final InventoryView view;
    /** A {@link Map} of {@link GUIItem}s linked to slots. */
    private Map<Integer, GUIItem> slots;

    public GUIView(GUI gui, InventoryView view, Map<Integer, GUIItem> slots) {
        this.gui = gui;
        this.view = view;
        this.slots = slots;
    }

    /** Gets the {@link GUI}.
     @return The {@link GUI}.
     */
    public GUI getGUI() { return gui; }

    /** Gets the {@link InventoryView}.
     @return The {@link InventoryView}.
     */
    public InventoryView getView() { return view; }

    /** Gets a {@link Map} of {@link GUIItem}s linked to slots.
     @return A {@link Map} of {@link GUIItem}s linked to slots.
     */
    public Map<Integer, GUIItem> getSlots() { return slots; }

    /**
     Sets the {@link Map} of {@link GUIItem}s linked to slots.
     @param slots The new {@link Map} of {@link GUIItem}s linked to slots.
     @param player The player that this is being generated to.
     */
    public void setSlots(Map<Integer, GUIItem> slots, Player player) {
        this.slots = slots;
        updateSlots(player);
    }

    /** Replaces all slots in the inventory with the new ones as defined by {@link GUIView#slots}.
     @param player The player that this is being generated to.
     */
    public void updateSlots(Player player) {
        view.getTopInventory().clear();
        for (int slot : slots.keySet())
            view.getTopInventory().setItem(slot, slots.get(slot).getHandle(player));
    }
}
