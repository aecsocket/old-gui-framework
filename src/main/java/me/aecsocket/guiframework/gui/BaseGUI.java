package me.aecsocket.guiframework.gui;

import me.aecsocket.guiframework.GUIFramework;
import me.aecsocket.guiframework.util.SizeType;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;

import java.util.Map;

/** A helper class around {@link GUI}. */
public abstract class BaseGUI implements GUI {
    /** Gets the title of the {@link GUI} for a particular {@link Player}.
     @param player The {@link Player}.
     @return The title.
     */
    public abstract String getTitle(Player player);

    /** Gets the {@link SizeType} of the {@link GUI} for a particular {@link Player}.
     @param player The {@link Player}.
     @return The {@link SizeType}.
     */
    public abstract SizeType getSizeType(Player player);

    /** Gets the {@link Map} of slots of the {@link GUI} for a particular {@link Player}.
     @param player The {@link Player}.
     @return The {@link Map} of slots.
     */
    public abstract Map<Integer, GUIItem> getSlots(Player player);

    @Override
    public GUIView open(Player player) {
        SizeType sizeType = getSizeType(player);
        Inventory inv;
        if (sizeType.getSelection() == SizeType.Selection.ROWS)
            inv = Bukkit.createInventory(player, sizeType.getRows() * 9, getTitle(player));
        else
            inv = Bukkit.createInventory(player, sizeType.getType(), getTitle(player));

        Map<Integer, GUIItem> slots = getSlots(player);

        InventoryView view = player.openInventory(inv);
        GUIView guiView = new GUIView(this, view, slots);

        guiView.updateSlots(player);
        GUIFramework.getViews().put(view, guiView);
        return guiView;
    }
}
