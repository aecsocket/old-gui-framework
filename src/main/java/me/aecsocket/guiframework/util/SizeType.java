package me.aecsocket.guiframework.util;

import org.bukkit.event.inventory.InventoryType;

/** A data container for an inventory's size. */
public class SizeType {
    /** Whether a {@link SizeType} has its rows or its type set. */
    public enum Selection {
        /** The inventory created will be made of rows. */
        ROWS,
        /** The inventory created will have the same size as another inventory type. */
        TYPE
    }

    /** The number of rows. */
    private int rows = 0;
    /** The {@link InventoryType}. */
    private InventoryType type = null;
    /** Whether this {@link SizeType} has its rows or its type set. */
    private final Selection selection;

    /** Sets this instance's rows.
     @param rows The number of rows.
     */
    public SizeType(int rows) {
        this.rows = rows;
        this.selection = Selection.ROWS;
    }

    /** Sets this instance's type.
     @param type The {@link InventoryType}.
     */
    public SizeType(InventoryType type) {
        this.type = type;
        this.selection = Selection.TYPE;
    }

    /** Gets whether this {@link SizeType} has its rows or its type set.
     @return Whether this {@link SizeType} has its rows or its type set.
     */
    public Selection getSelection() { return selection; }

    /** Gets the rows.
     @return The rows.
     */
    public int getRows() { return rows; }

    /** Gets the {@link InventoryType}.
     @return The {@link InventoryType}.
     */
    public InventoryType getType() { return type; }
}
