package me.aecsocket.guiframework;

import me.aecsocket.guiframework.gui.GUIItem;
import me.aecsocket.guiframework.gui.GUIView;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;

import java.util.Map;

public class EventHandle implements Listener {
    @EventHandler
    public void onClick(InventoryClickEvent event) {
        GUIView view = GUIFramework.getView(event.getView());
        if (view != null) {
            Inventory inv = event.getClickedInventory();
            ClickType click = event.getClick();
            if (view.getView().getTopInventory().equals(inv)
                    || click == ClickType.SHIFT_LEFT
                    || click == ClickType.SHIFT_RIGHT) {
                event.setCancelled(true);
                int slot = event.getRawSlot();

                Map<Integer, GUIItem> slots = view.getSlots();
                if (slots.containsKey(slot))
                    slots.get(slot).onClick(event, view);
            }
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        GUIView view = GUIFramework.getView(event.getView());
        if (view != null)
            GUIFramework.getViews().remove(view.getView());
    }

    @EventHandler
    public void onDrag(InventoryDragEvent event) {
        GUIView view = GUIFramework.getView(event.getView());
        if (view != null) {
            event.setCancelled(true);
        }
    }
}
