# GUIFramework

Create and manage GUI inventories with clickable items

---

This plugin allows creating custom inventories which act as menus, and allows creating classes to specify what clicking
a GUI item does.

## For server owners

Put the JAR in `$SERVER/plugins` and it will work if any plugins require it.

## For developers

Javadoc is available at [src/main/javadoc](src/main/javadoc).

Create a class implementing `GUI` or extending `BaseGUI` to create a GUI. You can then create a new instance of this
and use `GUI#open(Player)` to open the inventory to a player, returning a `GUIView`.

To add clickable items, create a class implementing `GUIItem` and in the resulting `GUIView` of `GUI#open(Player)`, set
the `GUIView`'s slots to a `Map<Integer, GUIItem>` of your clickable items.

If you use `BaseGUI`, this will already be added for you. Just override `BaseGUI#getSlots(Player)`.

Example:

```java
// package...
// import...

public class ShopItem implements GUIItem {
    private Material product;
    private int cost;
    
    public ShopItem(Material product, int cost) {
        this.product = product;
        this.cost = cost;
    }

    @Override
    public ItemStack getHandle() {
        ItemStack result = new ItemStack(product);
        ItemMeta meta = result.getItemMeta();

        meta.setDisplayName("Cost: " + cost);

        result.setItemMeta(meta);
        return result;
    }

    @Override
    public void onClick(InventoryClickEvent event, GUIView view) {
        Player player = (Player)event.getWhoClicked();
        player.sendMessage("You spent " + cost);
        player.getInventory().addItem(new ItemStack(product));
    }
}
```

```java
// package...
// import...

public class ShopGUI extends BaseGUI {
    @Override
    public String getTitle(Player player) { return "Shop"; }
    @Override
    public SizeType getSizeType(Player player) { return new SizeType(3); }
    @Override
    public Map<Integer, GUIItem> getSlots(Player player) {
        Map<Integer, GUIItem> map = new HashMap<>();

        map.put(0, new ShopItem(Material.COBBLESTONE, 5));
        map.put(1, new ShopItem(Material.OAK_LOG, 10));

        return map;
    }
}
```

## Download

Currently there is no JAR download available. You must compile it yourself.
